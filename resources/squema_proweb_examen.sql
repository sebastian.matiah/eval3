CREATE TABLE IF NOT EXISTS public.carrito
(
    id bigint NOT NULL DEFAULT nextval('"Carrito_id_seq"'::regclass),
    id bigint NOT NULL DEFAULT nextval('"Carrito_id_seq"'::regclass),
    codigo_producto integer,
    cantidad integer,
    CONSTRAINT "Carrito_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.carrito
    OWNER to postgres;

ALTER SEQUENCE Carrito_id_seq
OWNED BY carrito.id;


CREATE TABLE IF NOT EXISTS public.productos
(
    codigo integer NOT NULL DEFAULT nextval('productos_codigo_seq'::regclass),
    nombre character varying(255) COLLATE pg_catalog."default" NOT NULL,
    precio bigint,
    url text COLLATE pg_catalog."default",
    CONSTRAINT productos_pkey PRIMARY KEY (codigo)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.productos
    OWNER to postgres;

ALTER SEQUENCE productos_codigo_seq
OWNED BY productos.codigo;


CREATE TABLE IF NOT EXISTS public.registro
(
    codigo integer NOT NULL DEFAULT nextval('registro_codigo_seq'::regclass),
    email character varying(80) COLLATE pg_catalog."default",
    "contraseña" character varying(80) COLLATE pg_catalog."default",
    "rcontraseña" character varying(80) COLLATE pg_catalog."default",
    nombres character varying(80) COLLATE pg_catalog."default",
    apellidos character varying(80) COLLATE pg_catalog."default",
    run numeric(8,0),
    dv character varying(1) COLLATE pg_catalog."default",
    dia_nacimiento character varying(2) COLLATE pg_catalog."default",
    mes_nacimiento character varying(80) COLLATE pg_catalog."default",
    anio_nacimiento character varying(4) COLLATE pg_catalog."default",
    region character varying(80) COLLATE pg_catalog."default",
    comuna character varying(80) COLLATE pg_catalog."default",
    ciudad character varying(80) COLLATE pg_catalog."default",
    CONSTRAINT registro_pkey PRIMARY KEY (codigo)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.registro
	OWNER to postgres;

ALTER SEQUENCE registro_codigo_seq
OWNED BY registro.codigo;


CREATE TABLE IF NOT EXISTS public.contacto
(
    codigo bigint NOT NULL DEFAULT nextval('contacto_codigo_seq'::regclass),
    run numeric(13,0),
    dv character varying(80) COLLATE pg_catalog."default",
    nombres character varying(80) COLLATE pg_catalog."default",
    apellido_paterno character varying(80) COLLATE pg_catalog."default",
    apellido_materno character varying(80) COLLATE pg_catalog."default",
    email character varying(80) COLLATE pg_catalog."default",
    telefono character varying(16) COLLATE pg_catalog."default",
    asunto character varying(250) COLLATE pg_catalog."default",
    CONSTRAINT contacto_pkey PRIMARY KEY (codigo)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.contacto
    OWNER to postgres;

ALTER SEQUENCE contacto_codigo_seq
OWNED BY contacto.codigo;