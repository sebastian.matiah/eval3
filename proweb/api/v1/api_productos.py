from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view
from rest_framework import status
import json
from proweb.models import Producto

@api_view(['GET', 'POST'])
def productos(request):
    if request.method == 'GET':
        response_json = []
        productos = Producto.objects.all()       
        for producto in productos:
            js = {
                'codigo': int(producto.codigo),
                'nombre' : producto.nombre,
                'precio' : int(producto.precio),
                'url' : producto.url
            }
            response_json.append(js)
        status_code = status.HTTP_200_OK
        print('response_json', response_json)
        return HttpResponse(json.dumps(response_json, ensure_ascii=False), content_type="application/json", status=status_code)            
