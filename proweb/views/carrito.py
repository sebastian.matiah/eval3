from django.shortcuts import render
from proweb.models import Carrito
from proweb.models import Producto
import json

def cargar_carrito():
    carrito = Carrito.objects.all()
    all_carrito = []
    if carrito:
        for c in carrito:
            producto = Producto.objects.get(pk=c.codigo_producto)
            all_carrito.append({'id_cart': c.id, 'codigo': c.codigo_producto, 'cantidad': c.cantidad, 'precio': producto.precio, 'url': producto.url, 'nombre': producto.nombre })

    return all_carrito
def total_pago():
    carrito = cargar_carrito()
    total = 0;
    for c in carrito:
        total = total + (c['cantidad'] * c['precio'])
    return total

def carrito_index(request):
     if request.method == 'GET':
        try:
            id_cart = request.GET['id_cart']
            carrito = Carrito.objects.get(pk=id_cart)
            carrito.delete()
        except Exception as e:
            print(e)
        return render(request, 'carrito.html', {'carrito': cargar_carrito(), 'total': total_pago()})
