from django.shortcuts import render
from proweb.models import Carrito


def compra_index(request):
    carrito = Carrito.objects.all()
    carrito.delete()
    return render(request, 'compra.html')
