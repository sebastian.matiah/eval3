from django.shortcuts import render
import requests
from proweb.models import Carrito

def cargar_productos():
    api_url = "http://localhost:8000/api/v1/productos"
    response = requests.get(api_url)
    json_request = response.json()
    productos = json_request
    return productos

def galeria_index(request):
    return render(request, 'galeria.html', {'productos': cargar_productos()})

def add_cart(request):
    # if request.method == 'GET':
    try:
        codigo_producto = request.GET.get('codigo')
        existe_producto = Carrito.objects.filter(codigo_producto=codigo_producto)
        print("add_cart existe_producto: ", existe_producto)
        print("add_cart existe_producto TYPE: ", type(existe_producto))

        print("check ", existe_producto.exists())
        if existe_producto.exists():
            update_carrito = Carrito.objects.get(codigo_producto=codigo_producto)
            update_carrito.cantidad += 1
            update_carrito.save()
        else:
            carrito = Carrito()
            carrito.codigo_producto = int(codigo_producto)
            carrito.cantidad = 1
            carrito.save()
    except Exception as ex:
        print('Error ', ex)
    return render(request, 'galeria.html',  {'productos': cargar_productos()})
